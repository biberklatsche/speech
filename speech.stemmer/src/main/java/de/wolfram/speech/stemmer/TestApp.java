package de.wolfram.speech.stemmer;

import de.wolfram.speech.stemmer.ext.GermanStemmer;

public class TestApp
{

    public static void main(String[] args) throws Throwable
    {
        SnowballStemmer stemmer = new GermanStemmer();
        stemmer.setCurrent("youtube.com");
        stemmer.stem();
        System.out.println(stemmer.getCurrent());
    }
}
