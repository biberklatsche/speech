
package de.wolfram.speech.stemmer;

public abstract class SnowballStemmer extends SnowballProgram {
    public abstract boolean stem();
};
