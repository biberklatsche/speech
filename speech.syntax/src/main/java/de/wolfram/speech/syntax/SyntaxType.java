package de.wolfram.speech.syntax;

/**
 *
 * @author Lars
 */
public enum SyntaxType {
    OBJECT, PREDICATE, SUBJECT, OTHER;
}
