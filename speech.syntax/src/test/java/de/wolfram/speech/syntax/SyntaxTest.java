/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.wolfram.speech.syntax;

import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lars
 */
public class SyntaxTest {
    
    public SyntaxTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreate_Null() {
        String s = null;
        Syntax instance = new Syntax();
        List<SyntaxResult> result = instance.create(s);
        assertThat(result.size(), is(0));
    }
    
    @Test
    public void testCreate_oneWord_size() {
        String s = "testen";
        Syntax instance = new Syntax();
        List<SyntaxResult> result = instance.create(s);
        assertThat(result.size(), is(1));
    }
    
    @Test
    public void testCreate_oneWord_isMorePredicate() {
        String s = "testen";
        Syntax instance = new Syntax();
        List<SyntaxResult> result = instance.create(s);
        assertThat(result.get(0), is(1));
    }
    
}
