/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.wolfram.speech.stopwords;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wolframl
 */
class StopWords
{

    private HashSet<String> stopWords = new HashSet();
    
    void load()
    {
        try(
            InputStream stopWordsStream = getClass().getClassLoader().getResourceAsStream("stopwords.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stopWordsStream));
            )
        {
            String line;
            while(null != (line = reader.readLine())){
                stopWords.add(line);
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(StopWords.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String remove(String text)
    {
        String[] words = text.split("\\s");
        StringBuilder b = new StringBuilder();
        for(String word : words){
            if(!stopWords.contains(word)){
                if(b.length() > 0){
                    b.append(" ");
                }
                b.append(word);
            }
        }
        return b.toString();
    }
    
}
